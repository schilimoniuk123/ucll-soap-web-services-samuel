package be.ucll.java.ent.soap.server;
import be.ucll.java.ent.controller.StudentController;
import be.ucll.java.ent.domain.StudentDTO;
import be.ucll.java.ent.model.StudentEntity;
import be.ucll.java.ent.repository.StudentDAO;
import be.ucll.java.ent.rest.StudentsList;
import be.ucll.java.ent.soap.model.v1.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.sql.Connection;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

@Endpoint
public class StudentsEndpoint {

    @Autowired
    private StudentController studentController;

    @Autowired
    private StudentDAO dao;


    @PayloadRoot(namespace = "http://ucll.be/java/ent/students", localPart = "GetStudentsRequest")
    @ResponsePayload
    public GetStudentsResponse getStudent(@RequestPayload GetStudentsRequest request) throws IllegalArgumentException, DatatypeConfigurationException {

        GetStudentsResponse response = new GetStudentsResponse();
        StudentDTO studentDTO;

        if ((request.getName() != null && !request.getName().isEmpty()) && request.getId() == null)
        {

            try
            {
                studentDTO = studentController.getStudentByName(request.getName());

                System.out.println("naam is gevonden");
                dataAquiring(studentDTO, response);
                response.setCode(0);
                response.setType(STypeProcessOutcome.INFO);
            } catch(IllegalArgumentException e)
            {
                System.out.println("naam niet gevonden");
                response.setCode(1);
                response.setType(STypeProcessOutcome.WARNING);
                response.setErrormessage("naam niet gevonden");
            } catch(Exception e)
            {
                System.out.println(e.getMessage());
                e.printStackTrace();
                System.out.println(e.getClass());
                response.setCode(1);
                response.setType(STypeProcessOutcome.ERROR);
                response.setErrormessage("er beging een fout");
            }

        } else if (request.getId() != null && request.getName().isEmpty()) {
            try
            {
                studentDTO = studentController.getStudentById(request.getId());

                System.out.println("id is gevonden");
                dataAquiring(studentDTO, response);
                response.setCode(0);
                response.setType(STypeProcessOutcome.INFO);
            } catch(IllegalArgumentException e)
            {
                System.out.println("id niet gevonden");
                response.setCode(1);
                response.setType(STypeProcessOutcome.WARNING);
                response.setErrormessage("id niet gevonden");
            } catch(Exception e)
            {
                System.out.println(e.getMessage());
                e.printStackTrace();
                System.out.println(e.getClass());
                response.setCode(1);
                response.setType(STypeProcessOutcome.ERROR);
                response.setErrormessage("er beging een fout");
            }

        } else {
            System.out.println("error");
            response.setCode(1);
            response.setType(STypeProcessOutcome.ERROR);
            response.setErrormessage("give a right parameter");
        }
        return response;
    }

    public void dataAquiring(StudentDTO studentDTO,GetStudentsResponse response) throws IllegalArgumentException, DatatypeConfigurationException
    {
        CTypeStudent cTypeStudent = new CTypeStudent();
        CTypeStudents cTypeStudents = new CTypeStudents();

        GregorianCalendar c = new GregorianCalendar();
        c.setTime(studentDTO.getGeboortedatum());
        XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
        cTypeStudent.setNaam(studentDTO.getNaam());
        cTypeStudent.setGeboortedatum(date2);
        cTypeStudent.setVoornaam(studentDTO.getVoornaam());

        List<CTypeStudent> ctsList = cTypeStudents.getStudent();
        ctsList.add(cTypeStudent);
        response.setStudents(cTypeStudents);
    }

}
